// Cuando se carga el documento
window.onload = function () {
  // Precarga elementos
  queue.loadManifest([
    {id: 'video', src: '../../video/pan.mp4'},
  ]);

  // Navegación
  queue.on('complete', function () {
    init_nav([
      {
        parent: document.getElementsByTagName('video')[0],
        x: .43,
        y: .85,
        width: 100,
        height: 100,
        label: 'garbige',
        href: '../garbige/index.html',
        vidTime: 137.4,
        life: 7.2
      },
      {
        parent: document.getElementsByTagName('video')[0],
        x: .5,
        y: .5,
        width: 100,
        height: 100,
        label: 'garbige',
        href: '../garbige/index.html',
        vidTime: 158,
        life: 4
      }
    ]);
  }, this);
}
