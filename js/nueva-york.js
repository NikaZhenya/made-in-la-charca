// Cuando se carga el documento
window.onload = function () {
  // Precarga elementos
  queue.loadManifest([
    {id: 'video', src: '../../video/nueva-york.mp4'},
  ]);

  // Navegación
  queue.on('complete', function () {
    init_nav([
      {
        parent: document.getElementsByTagName('video')[0],
        x: .625,
        y: .45,
        width: 100,
        height: 100,
        label: 'index',
        href: '../../index.html',
        vidTime: 154,
        life: 2.5
      }
    ]);
  }, this);
}
