// Cuando se han cargado los elementos
function preload_complete (e) {
  play_audio('sound', true);
  insert_video_full();
}

// Cuando se carga el documento
window.onload = function () {
  // Precarga elementos
  queue.loadManifest([
    {id: 'sound', src: 'sound/intro.mp3'},
    {id: 'video', src: 'video/intro.mp4'},
  ]);

  // Navegación
  queue.on('complete', function () {
    init_nav([
      {
        parent: document.getElementsByTagName('div')[0],
        x: .05,
        y: .9,
        width: 50,
        height: 50,
        label: 'info',
        href: 'html/info/index.html',
        vidTime: 1,
        life: 3
      },
      {
        parent: document.getElementsByTagName('div')[0],
        x: .6,
        y: .66,
        width: 100,
        height: 100,
        label: 'como una película',
        href: 'html/como-una-pelicula/index.html',
        vidTime: 7,
        life: 3
      },
      {
        parent: document.getElementsByTagName('div')[0],
        x: .5,
        y: .5,
        width: 100,
        height: 100,
        label: 'calor',
        href: 'html/calor/index.html',
        vidTime: 15,
        life: 2
      },
      {
        parent: document.getElementsByTagName('div')[0],
        x: .8,
        y: .3,
        width: 100,
        height: 100,
        label: 'la-boda',
        href: 'html/la-boda/index.html',
        vidTime: 20,
        life: 3
      }
    ]);
  }, this);
}
