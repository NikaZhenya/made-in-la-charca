// Cuando se han cargado los elementos
function preload_complete (e) {
  play_audio('sound', true);
}

// Cambia al reverso o reverso de la imagen
function change_img (id) {
  var e     = document.getElementById(id),
      img   = e.getElementsByClassName('masonry-content');

  if (img.length > 1) {
    for (var i = 0; i < img.length; i++) {
      var display = img[i].style.display == 'initial' ? 'none' : 'initial';

      img[i].style.display = display;
    }
  }
}

// Cuando se carga el documento
window.onload = function () {
  var items = document.getElementsByClassName('masonry-item');

  // Precarga elementos
  queue.loadManifest([
    {id: 'sound', src: '../../sound/fotos-recuerdos.mp3'},
  ]);

  // Agrega escuchas
  for (var i = 0; i < items.length; i++) {
    var item    = items[i];

    item.addEventListener('mouseover', function(){change_img(this.id)});
    item.addEventListener('mouseout', function(){change_img(this.id)});
    item.addEventListener('touch', function(){change_img(this.id)});
  }

  // Navegación
  queue.on('complete', function () {
    init_nav([
      {
        parent: document.getElementById('item-1'),
        x: .5,
        y: .8,
        width: 100,
        height: 100,
        label: 'irma',
        href: '../irma/index.html',
        life: 4
      },
      {
        parent: document.getElementById('item-12'),
        x: .5,
        y: .5,
        width: 100,
        height: 100,
        label: 'nueva york',
        href: '../nueva-york/index.html',
        life: 4
      }
    ]);
  }, this);
}
