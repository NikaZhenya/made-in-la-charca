// Cuando se carga el documento
window.onload = function () {
  // Precarga elementos
  queue.loadManifest([
    {id: 'video', src: '../../video/irma.mp4'},
  ]);

  // Navegación
  queue.on('complete', function () {
    init_nav([
      {
        parent: document.getElementsByTagName('div')[0],
        x: .5,
        y: .87,
        width: 100,
        height: 100,
        label: 'nueva york',
        href: '../nueva-york/index.html',
        vidTime: 102,
        life: 4
      }
    ]);
  }, this);
}
