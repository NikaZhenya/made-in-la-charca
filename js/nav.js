var nav_resize = false;
    nav_debug  = false;

// Muestra la navegación
function init_nav (pointers, index, debug) {
  var index = index || 0,
      debug = debug || false,
      wait  = 3000;

  // Evitará eliminar los elementos para poder depurar sus configuraciones
  nav_debug = debug

  // Crea el elemento
  function create_pointer (i, els) {
    if (!document.getElementById('nav-' + (i + 1))) {
      var el = els[i],
          r  = el.parent.getBoundingClientRect()
          x  = document.documentElement.scrollLeft + r.left + ((el.parent.offsetWidth  * el.x) - (el.width  / 2)),
          y  = document.documentElement.scrollTop  + r.top  + ((el.parent.offsetHeight * el.y) - (el.height / 2)),
          a  = document.createElement('a');

      a.id = 'nav-' + (i + 1);
      a.innerHTML = el.label;
      a.href = el.href;
      a.style.position = 'absolute';
      a.style.top = y + 'px';
      a.style.left = x + 'px';
      a.style.display = 'block';
      a.style.width = (el.width - 12) + 'px';
      a.style.height = (el.height - 12) + 'px';
      a.style.padding = '5px';
      a.style.fontSize = '.5em';
      a.style.color = 'yellow';
      a.style.border = '1px solid yellow';
      a.style.zIndex = 9999999;
      a.classList.add('nav-item');
      document.body.appendChild(a);

      // Elimina el contenido
      setTimeout(destroy_pointer, el.life * 1000, a.id);
    }
  }

  // Destruye el elemento
  function destroy_pointer (id) {
    if (document.getElementById(id) && !nav_debug) {
      var e = document.getElementById(id);

      e.parentNode.removeChild(e);
    }
  }

  // Crea el siguiente elemento
  function next_pointer (i) {
    if (!nav_debug) {
      if (i == pointers.length - 1) {
        init_nav(pointers, 0);
      } else {
        init_nav(pointers, i + 1);
      }
    }
  }

  // Si es para sincronizar con video
  if (pointers[0].vidTime) {
    // Analiza la información cada 100 ms
    setInterval(function () {
      var video = document.getElementsByTagName('video')[0];

      // Iteración de todos los elementos
      for (var j = 0; j <= pointers.length - 1; j++) {
        var pointer = pointers[j];

        // Crea el elemento al estar en el rango de tiempo
        if (pointer.vidTime >= video.currentTime - 0.1 &&
            pointer.vidTime <= video.currentTime + 0.1) {
          create_pointer(j, pointers);
        }
      }
    }, 100);
  // Si no es para sincronizar con video
  } else {
    create_pointer(index, pointers);
    setTimeout(next_pointer, wait + (pointers[index].life * 1000), index);
  }

  // Alinea elementos cuando se cambia el tamaño de pantalla
  window.addEventListener('resize', function () {
    if (!nav_resize) {
      var items = document.getElementsByClassName('nav-item');

      // Funciona como freno
      nav_resize = true;

      // Elimina los elementos de la navegación previos
      while (items.length > 0) {
        items[0].parentNode.removeChild(items[0]);
      }

      // Reinicia la navegación
      init_nav(pointers);

      // Quita el freno en 500 ms
      setTimeout(function () {
        nav_resize = false;
      }, 500);
    }
  });
}
