// Cuando se carga el documento
window.onload = function () {
  var panel = document.getElementById('panel'),
      total = panel.getAttribute('panels');

  // Crea los panelitos según la cantidad de enlaces
  for (var i = 0; i < total; i++) {
    var div   = document.createElement('div'),
        width = 100 / total;

    div.id = 'panel-' + i;
    div.style.width = width + '%';
    panel.appendChild(div);

    // Añade un escucha cuando el ratón esté encima del panelito
    div.addEventListener('mouseover', function (e) {
      var id = parseInt(e.target.id.split('-')[1]),
          a  = document.getElementsByClassName('panels');

      for (var j = 0; j < a.length; j++) {
        // Si el panelito coincide con el enlace, lo resalta
        if (j == id) {
          a[j].style.zIndex  = 10;
          a[j].style.opacity = 0.75;
        // De lo contrario, lo relega
        } else {
          a[j].style.zIndex  = 0;
          a[j].style.opacity = 0.5;
        }
      }
    });

    // Añade un escucha para abrir enlaces
    div.addEventListener('click', function (e) {
      var id = parseInt(e.target.id.split('-')[1]),
          a  = document.getElementsByClassName('panels');

      // La url corresponda al que esté en el enlace
      for (var j = 0; j < a.length; j++) {
        if (j == id) {
          window.open(a[j].getAttribute('href'));
        }
      }
    });
  }

  // Resetea los resaltes
  document.addEventListener('mousemove', function (e) {
    var condition = false,
        a = document.getElementsByClassName('panels');

      // Cuando está en horizontal
      if (window.innerWidth >= window.innerHeight) {
        // Tiene que estar en el primer tercio de la pantalla
        if (e.pageX <= parseInt(window.innerWidth * .3)) {
          condition = true;
        }
      // Cuando está en vertical
      } else {
        // Tiene que estar en la primera mitad de la pantalla
        if (e.pageY <= parseInt(window.innerHeight / 2)) {
          condition = true;
        }
      }

    if (condition) {
      for (var j = 0; j < a.length; j++) {
        a[j].style.zIndex  = 0;
        a[j].style.opacity = 0.5;
      }
    }
  });
}
