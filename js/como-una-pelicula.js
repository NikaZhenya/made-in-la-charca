// Cuando se carga el documento
window.onload = function () {
  // Precarga elementos
  queue.loadManifest([
    {id: 'video', src: '../../video/como-una-pelicula.mp4'},
  ]);

  // Navegación
  queue.on('complete', function () {
    init_nav([
      {
        parent: document.getElementsByTagName('div')[0],
        x: .5,
        y: .48,
        width: 100,
        height: 100,
        label: 'pan',
        href: '../pan/index.html',
        vidTime: 59.7,
        life: 3.6
      },
      {
        parent: document.getElementsByTagName('div')[0],
        x: .5,
        y: .48,
        width: 100,
        height: 100,
        label: 'pan',
        href: '../pan/index.html',
        vidTime: 105.6,
        life: 2.4
      }
    ]);
  }, this);
}
