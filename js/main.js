var queue = new createjs.LoadQueue(),
    timer;

// Para precargar elementos
queue.installPlugin(createjs.Sound);
queue.on('progress', queue_progress, this);
queue.on('complete', queue_complete, this);
queue.on('error', queue_error, this);

// Muestra barra de progreso de carga
function queue_progress (e) {
  var status   = parseInt(e.progress * 100),
      progress = document.getElementById('progress');

  progress.innerHTML = status + '%';
}

// Cuando se han cargado los elementos
function queue_complete (e) {
  var load    = document.getElementById('load'),
      content = document.getElementById('content');

  content.style.display = 'initial';
  load.parentNode.removeChild(load);
  preload_complete(e);
}

// Cuando hubo error en la carga
function queue_error (e) {
  var progress = document.getElementById('progress');

  progress.innerHTML = 'Error al cargar :O';
}

// Reproduce el audio
function play_audio (id, loop) {
  var loop  = loop || false,
      sound = createjs.Sound.play(id);

  if (loop) {
    sound.on('complete', function () {
      this.currentTime = 0;
      this.play();
    });
  }
}

// Coloca un video en tamaño completo
function insert_video () {
  var content = document.getElementById('content'),
      div     = document.createElement('div'),
      video   = queue.getResult('video');

  // Genera un loop en el video; de: https://stackoverflow.com/questions/3455229/control-html5-video-player-loop-with-javascript 
  if (typeof video.loop == 'boolean') {
    video.loop = true;
  } else { // loop property not supported
    video.addEventListener('ended', function () {
      this.currentTime = 0;
      this.play();
    }, false);
  }

  // Crea los elementos
  div.classList.add('container-video');
  video.classList.add('video');
  div.appendChild(video)
  content.appendChild(div);

  // Configura el video
  video.muted = true;
  video.autoplay = true;
  video.load();
}

// Coloca un video en tamaño completo
function insert_video_full () {
  var content = document.getElementById('content'),
      div     = document.createElement('div'),
      video   = queue.getResult('video');

  // Genera un loop en el video; de: https://stackoverflow.com/questions/3455229/control-html5-video-player-loop-with-javascript 
  if (typeof video.loop == 'boolean') {
    video.loop = true;
  } else { // loop property not supported
    video.addEventListener('ended', function () {
      this.currentTime = 0;
      this.play();
    }, false);
  }

  // Añade identificadores
  div.id = 'container-video-full';
  video.id = 'video-full';

  // Crea los elementos
  div.classList.add('full');
  video.classList.add('full');
  div.appendChild(video)
  content.appendChild(div);

  // Configura el video
  video.muted = true;
  video.autoplay = true;
  video.load();

  // Añade escucha para redimensión del video
  video.addEventListener('loadedmetadata', resize_video_full, false);
  window.addEventListener('resize', resize_video_full, false);
}

// Cambia el tamaño del video para que siempre esté centrado; de: http://codetheory.in/html5-fullscreen-background-video/
function resize_video_full () {
  var container = document.getElementById('container-video-full'),
      video     = document.getElementById('video-full'),
      c_styles  = window.getComputedStyle(container),
      w         = video.videoWidth,
      h         = video.videoHeight,
      w_min     = parseInt(c_styles.getPropertyValue('width')),
      h_min     = parseInt(c_styles.getPropertyValue('height')),
      w_ratio   = w_min / w,
      h_ratio   = h_min / h,
      v_ratio   = (w / h).toFixed(2),
      w_new,
      h_new;

  // Otiene tamaños nuevos según la relación de aspecto
  if (w_ratio > h_ratio) {
    w_new = w_min;
    h_new = Math.ceil(w_new / v_ratio);
  } else {
    w_new = Math.ceil(h_new * v_ratio );
    h_new = h_min;
  }

  // Cambia tamaños de video
  video.style.width  = w_new + 'px';
  video.style.height = h_new + 'px';
};

// Muestra los subtitulos
function subtitles (class_name, content, video_needed) {
    var time;

  // Redondea a un decimal
  function round_num (num) {
    return Math.round(num * 10) / 10;
  }

  // Inicia subtítulos
  function init_subs () {
    // Inicia un intervalo cada 0.1 s
    timer = setInterval(function() {

      // El tiempo es o el tiempo actual del video o una suma del intervalo
      if (video_needed) {
        time = round_num(document.getElementsByTagName('video')[0].currentTime);
      } else {
        if (typeof time === 'undefined') {
          time = 0.0;
        } else {
          if (time > content[content.length - 1].end) {
            time = 0.0;
          }
        }
      }

      // Crea u obtiene el párrafo donde van los subtítulos
      if (document.getElementById('subtitles')) {
        var p = document.getElementById('subtitles');
      } else {
        var p = document.createElement('p');
            p.id = 'subtitles';
            document.body.appendChild(p);
      }

      // Iteración de los sutítulos
      for (var i = 0; i < content.length; i++) {
        var c = content[i];

        // Si uno de los contenidos empieza cerca del tiempo actual
        if (c.start >= round_num(time - 0.1) &&
            c.start <= round_num(time + 0.1)) {
 
          // Si no existe, se añade
          if (!document.getElementById('sub-' + i)) {
            var sub_new = document.createElement('span');
                sub_new.id = 'sub-' + i;
                sub_new.innerHTML = c.text;
                sub_new.classList.add(class_name);
                p.appendChild(sub_new);
          }
        }

        // Si uno de los contenidos termina cerca del tiempo actual
        if (c.end >= round_num(time - 0.1) &&
            c.end <= round_num(time + 0.1)) {

          // Si existe, se elimina
          if (document.getElementById('sub-' + i)) {
            var sub_old = document.getElementById('sub-' + i);
                sub_old.parentNode.removeChild(sub_old);
          }
        }
      }

      // Suma al timer si no hay video
      if (!video_needed) {
        time += 0.1;
      }
    }, 100);
  }

  // Si se necesita video
  if (video_needed) {

    // Si no ha cargó
    if (document.getElementsByTagName('video').length == 0) {
      setTimeout(function(){subtitles(class_name, content, video_needed);}, 100);
    // Si ya cargó
    } else {
      init_subs();
    }
  // Si no se necesita video
  } else {
    init_subs();
  }
}

// Imágenes cambian en ruleta
function roulette (id, images, delay) {
  var interval = 4000,
      div      = document.getElementById(id),
      style    = div.getAttribute('style'),
      link;

  // Si aún no hay imagen de fondo
  if (style == null) {
    url  = images[0][0];
    link = images[0][1];
  // Si ya hay imagen
  } else {
    var url_img  = style.split(/url\(["|']/)[1].split(/["|']\)/)[0].split(/\//),
        curr_img = url_img[url_img.length - 1],
        next_img;

    // Iteración para obtener la siguiente imagen
    for (var i = 0; i < images.length; i++) {
      var img = images[i][0];

      // Si coincide la imagen actual con alguna de las imágenes
      if (img.trim() == curr_img.trim()) {

        // Si es la última del conjunto
        if (i >= images.length - 1) {
          next_img = images[0];
        // Si no es la última del conjunto
        } else {
          next_img = images[i + 1];
        }

        break;
      }
    }

    url  = next_img[0];
    link = next_img[1];
  }

  // Hace el cambio de fondo
  div.style.background = "url('../../img/" + url + "')";

  // Adición de escucha para abrir link
  clone = div.cloneNode(true);
  div.parentNode.replaceChild(clone, div);
  clone.addEventListener('click', function () {
    window.open(link, '_self');
  });

  // En cierto intervalo vuelve a comenzar para cambiar de imagen
  setTimeout(function() {
    roulette(id, images, 0);
  }, interval + delay);
}
