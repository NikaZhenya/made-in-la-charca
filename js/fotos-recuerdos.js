// Cuando se han cargado los elementos
function preload_complete (e) {
  play_audio('sound', true);
}

// Cuando se carga el documento
window.onload = function () {
  // Precarga elementos
  queue.loadManifest([
    {id: 'sound', src: '../../sound/fotos-recuerdos.mp3'},
  ]);

  subtitles('fotos-recuerdos-subs', [
    {start: 0, end: 4, text: 'Tenía 38 años cuando decidí irme.'},
    {start: 4, end: 8, text: 'Las chiquillas tenían 15.'},
    {start: 8, end: 12, text: 'Mati tenía 16 años cuando se fue.'},
    {start: 12, end: 16, text: 'Él dice que le fue mal en eso.'},
    {start: 16, end: 20, text: 'Porque él se fue, lo agarraron los policías,'},
    {start: 20, end: 24, text: 'los que están en la aduana y estando ahí, lo soltaron.'},
    {start: 24, end: 28, text: 'Y él se iba a comer a la basura.'},
    {start: 28, end: 32, text: 'Así estuvo varias veces, no más imagínate que'},
    {start: 32, end: 36, text: 'no puede arreglar sus papeles,'},
    {start: 36, end: 40, text: 'porque tiene un pendiente así,'},
    {start: 40, end: 44, text: 'de que lo agarraron y lo agarraron…'},
    {start: 44, end: 48, text: 'En un mes, estuvo parece que 28 veces intentándolo'},
    {start: 48, end: 52, text: 'y las 28 veces lo agarraron.'},
    {start: 52, end: 56, text: 'Ahí en el cruce, donde cruza uno la frontera.'},
    {start: 56, end: 60, text: 'Ya estando ahí lo agarraban y lo soltaban en México'},
    {start: 60, end: 64, text: 'y él otra vez lo intentaba.'},
    {start: 64, end: 68, text: 'Y así estuvo y así estuvo.'},
    {start: 68, end: 72, text: 'Ya casi para pasar, dice que se iba y lo soltaban'},
    {start: 72, end: 76, text: 'y le decían que se regresara.'},
    {start: 76, end: 80, text: 'Regrésate con tu papá, regrésate con tu mamá…'},
    {start: 80, end: 84, text: 'Y el les decía: No, es que mi papá y mi mamá'},
    {start: 84, end: 88, text: 'están separados y por eso me vine, para ayudar a mi mamá.'}
  ], false);

  roulette('gallery-item-1',
    [ ['fotos-recuerdos-1.jpg', '../garbige/index.html'],
      ['fotos-recuerdos-2.jpg', '#2'],
      ['fotos-recuerdos-3.jpg', '#3']
    ], 0);

  roulette('gallery-item-2',
    [ ['fotos-recuerdos-4.jpg', '#4'],
      ['fotos-recuerdos-5.jpg', '#5'],
      ['fotos-recuerdos-6.jpg', '#6']
    ], 500);

  // Navegación
  queue.on('complete', function () {
    init_nav([
      {
        parent: document.body,
        x: .66,
        y: .5,
        width: 100,
        height: 100,
        label: 'la visa',
        href: '../la-visa/index.html',
        life: 4
      }
    ]);
  }, this);
}
