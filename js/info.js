// Cuando se carga el documento
function load_video () {
  // Precarga elementos
  queue.loadManifest([
    {id: 'video', src: '../../video/info.mp4'},
  ]);

  // Navegación
  queue.on('complete', function () {
    init_nav([
      {
        parent: document.getElementsByTagName('div')[0].getElementsByTagName('div')[0],
        x: .5,
        y: .9,
        width: 100,
        height: 100,
        label: 'index',
        href: '../../index.html',
        life: 4
      }
    ]);
  }, this);
}
